#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

#include "aimbot.h"

using namespace std;

extern uintptr_t clientBase;
extern uintptr_t engineBase;


int targetBone = 8;
bool lockHighlighting = true;
float highlightColorR = 0.0;
float highlightColorG = 2.0;
float highlightColorB = 0.0;
float fov = 5;


class Vec2 {
public:
	float x, y;

	Vec2 operator+(Vec2 d) {
		float tmpX = x + d.x;
		float tmpY = y + d.y;
		return { tmpX , tmpY };
	}
	Vec2 operator-(Vec2 d) {
		float tmpX = x - d.x;
		float tmpY = y - d.y;
		return { tmpX , tmpY };
	}
	Vec2 operator*(float d) {
		float tmpX = x * d;
		float tmpY = y * d;
		return { tmpX , tmpY };
	}
	void Normalize() {
		if (x > 89.0) {
			x -= 360.0;
		}
		if (x < -89.0) {
			x += 360.0;
		}
		if (y > 180.0) {
			y -= 360.0;
		}
		if (y < -180.0) {
			y += 360.0;
		}
	}
};


class Vec3 {
public:
	float x, y, z;

	Vec3 operator+(Vec3 d) {
		Vec3 rslt;
		float tmpX = x + d.x;
		float tmpY = y + d.y;
		float tmpZ = z + d.z;
		return { tmpX , tmpY , tmpZ };
	}
	Vec3 operator-(Vec3 d) {
		float tmpX = x - d.x;
		float tmpY = y - d.y;
		float tmpZ = z - d.z;
		return { tmpX , tmpY , tmpZ };
	}
	Vec3 operator*(float d) {
		float tmpX = x * d;
		float tmpY = y * d;
		float tmpZ = z * d;
		return { tmpX , tmpY , tmpZ };
	}
	void Normalize() {
		while (y > 180) {
			y -= 360;
		}
		while (y < -180) {
			y += 360;
		}
		if (x > 89) {
			x = 89;
		}
		if (x < -89) {
			x = -89;
		}
	}
	Vec2 calcAngles() {
		float pitch, yaw;
		float hypotenuse = sqrt(x * x + y * y + z * z);
		pitch = ((double)atan(z / hypotenuse) * 180.0) / M_PI;
		yaw = ((double)atan(y / x) * 180.0) / M_PI;
		if (x >= 0.0) {
			yaw += 180.0;
		}

		return { pitch, yaw };
	}
};


void aimbot() {
	uintptr_t localPlayer = *(uintptr_t*)(clientBase + dwLocalPlayer);
	if (localPlayer) {
		int localPlayerTeam = *(int*)(localPlayer + m_iTeamNum);
		try {
			uintptr_t enginePtr = *(uintptr_t*)(engineBase + dwClientState);
			float oldDistX = 11111111.0;
			float oldDistY = 11111111.0;

			for (int i = 1; i < maxPlayer; i++) {
				try {
					uintptr_t entity = *(uintptr_t*)(clientBase + dwEntityList + i * 0x10);

					int entityTeam, entityHealth;
					bool entityIsDormant;
					if (entity != 0) {
						try {
							entityTeam = *(int*)(entity + m_iTeamNum);
							entityHealth = *(int*)(entity + m_iHealth);
							entityIsDormant = *(bool*)(entity + m_bDormant);
						}
						catch (...) {
							continue;
						}

						int target = 0;
						int targetHealth = 0;
						bool targetIsDormant = true;
						Vec3 targetPos, localPos;

						if (localPlayerTeam != entityTeam && entityHealth > 0 && (entityTeam == 2 || entityTeam == 3)) {
							Vec3 localViewAngles = *(Vec3*)(*(uintptr_t*)(engineBase + dwClientState) + dwClientState_ViewAngles);
							localViewAngles.z = *(float*)(localPlayer + m_vecViewOffset + 0x8);

							localPos = *(Vec3*)(localPlayer + m_vecOrigin);
							localPos.z = *(float*)(localPlayer + m_vecOrigin + 0x8) + localViewAngles.z;

							uintptr_t entityBones = *(uintptr_t*)(entity + m_dwBoneMatrix);
							Vec3 entityPos;
							entityPos.x = *(float*)(entityBones + 0x30 * targetBone + 0x0C);
							entityPos.y = *(float*)(entityBones + 0x30 * targetBone + 0x1C);
							entityPos.z = *(float*)(entityBones + 0x30 * targetBone + 0x2C);

							Vec3 tmp = localPos - entityPos;
							Vec2 angleVec2 = tmp.calcAngles();

							float distX = angleVec2.x - localViewAngles.x;
							if (distX < -89.0) {
								distX += 360.0;
							}
							else if (distX > 89.0) {
								distX -= 360.0;
							}
							if (distX < 0.0) {
								distX = -distX;
							}
							float distY = angleVec2.y - localViewAngles.y;
							if (distY < -180.0) {
								distY += 360.0;
							}
							else if (distY > 180.0) {
								distY -= 360.0;
							}
							if (distY < 0.0) {
								distY = -distY;
							}

							if (distX < (oldDistX - 0.25) && distY < (oldDistY - 0.25) && distX <= fov && distY <= fov && distX) {
								if (lockHighlighting) {
									uintptr_t curGlowIndex = *(uintptr_t*)(entity + m_iGlowIndex);
									uintptr_t glowObj = *(uintptr_t*)(clientBase + dwGlowObjectManager);

									*(float*)(glowObj + curGlowIndex * 0x38 + 0x4) = highlightColorR;
									*(float*)(glowObj + curGlowIndex * 0x38 + 0x8) = highlightColorG;
									*(float*)(glowObj + curGlowIndex * 0x38 + 0xC) = highlightColorB;
								}

								oldDistX = distX;
								oldDistY = distY;
								target = entity;
								targetHealth = entityHealth;
								targetIsDormant = entityIsDormant;
								targetPos = entityPos;
							}
						}

						if (GetAsyncKeyState(VK_LBUTTON) < 0 && localPlayer != 0) {

							if (target != 0 && targetHealth > 0 && !targetIsDormant) {
								Vec3 tmp = localPos - targetPos;
								Vec2 angleVec2 = tmp.calcAngles();
								angleVec2.Normalize();

								*(float*)(enginePtr + dwClientState_ViewAngles) = angleVec2.x;
								*(float*)(enginePtr + dwClientState_ViewAngles + 0x4) = angleVec2.y;

								//Sleep(1);
							}
						}
					}
				}
				catch (...) {
					continue;
				}
			}
		}
		catch (...) {
			return;
		}
	}
}