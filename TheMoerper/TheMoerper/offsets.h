#pragma once
#include <Windows.h>


const DWORD m_bDormant = 0xED;
const DWORD m_iHealth = 0x100;
const DWORD m_vecOrigin = 0x138;
const DWORD m_iTeamNum = 0xF4;
const DWORD m_iCrosshairId = 0xB3E8;
const DWORD m_iGlowIndex = 0xA438;
const DWORD m_vecViewOffset = 0x108;
const DWORD m_dwBoneMatrix = 0x26A8;
const DWORD m_aimPunchAngle = 0x302C;
const DWORD m_hMyWeapons = 0x2DF8;
const DWORD m_iItemDefinitionIndex = 0x2FAA;
const DWORD m_nFallbackPaintKit = 0x31C8;
const DWORD m_flFallbackWear = 0x31D0;
const DWORD m_iItemIDHigh = 0x2FC0;
const DWORD m_nFallbackSeed = 0x31CC;
const DWORD m_nModelIndex = 0x258;
const DWORD m_iViewModelIndex = 0x3240;
const DWORD m_hActiveWeapon = 0x2EF8;
const DWORD m_hViewModel = 0x32F8;
const DWORD m_fFlags = 0x104;
const DWORD m_iShotsFired = 0xA390;


const DWORD dwEntityList = 0x4DA521C;
const DWORD dwViewMatrix = 0x4D96B34;
const DWORD dwLocalPlayer = 0xD8D2CC;
const DWORD dwForceAttack = 0x31D6750;
const DWORD dwGlowObjectManager = 0x52ED6A8;
const DWORD dwClientState = 0x589FE4;
const DWORD dwClientState_ViewAngles = 0x4D90;
const DWORD dwForceJump = 0x524EFFC;



const int maxPlayer = 32;