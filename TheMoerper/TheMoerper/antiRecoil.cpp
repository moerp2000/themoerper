#include "antiRecoil.h"
#include <iostream>
#include <stdio.h>

extern uintptr_t clientBase;
extern uintptr_t engineBase;

using namespace std;

struct Vec3 {
	float x, y, z;

	Vec3 operator+(Vec3 d) {
		return { x + d.x, y + d.y, z + d.z };
	}
	Vec3 operator-(Vec3 d) {
		return { x - d.x, y - d.y, z - d.z };
	}
	Vec3 operator*(float d) {
		return { x * d, y * d, z * d };
	}

	void Normalize() {
		while (y < -180) { y += 360; }
		while (y > 180) { y -= 360; }
		if (x > 89) { x = 89; }
		if (x < -89) { x = -89; }
	}
};



Vec3 oPunch{ 0, 0, 0 };
void antiRecoil() {
	uintptr_t localPlayer = *(uintptr_t*)(clientBase + dwLocalPlayer);
	Vec3* viewAngles = (Vec3*)(*(uintptr_t*)(engineBase + dwClientState) + dwClientState_ViewAngles);
	int* shotsFired = (int*)(localPlayer + m_iShotsFired);
	Vec3* aimPunchAnge = (Vec3*)(localPlayer + m_aimPunchAngle);

	//Vec3 oPunch{ 0, 0, 0 };


	Vec3 punchAngle = *aimPunchAnge * 2;
	if (*shotsFired > 1) {
		Vec3 nAngle = *viewAngles + oPunch - punchAngle;
		nAngle.Normalize();
		*viewAngles = nAngle;
	}
	oPunch = punchAngle;
}