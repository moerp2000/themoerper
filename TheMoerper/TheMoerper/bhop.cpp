#include "bhop.h"

extern uintptr_t clientBase;

void bhop() {
	uintptr_t localPlayer = *(uintptr_t*)(clientBase + dwLocalPlayer);
	if (localPlayer) {
		int flags = *(int*)(localPlayer + m_fFlags);

		if (flags & (1 << 0) && (GetAsyncKeyState(VK_SPACE) < 0)) {
			*(uintptr_t*)(clientBase + dwForceJump) = 6;
		}
	}
}