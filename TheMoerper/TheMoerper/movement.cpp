#include "movement.h"

using namespace std;

extern uintptr_t clientBase;


void bhop() {
	uintptr_t localPlayer = *(uintptr_t*)(clientBase + dwLocalPlayer);
	int flags = *(int*)(localPlayer + m_fFlags);

	if ((flags & (1 << 0) && GetAsyncKeyState(VK_SPACE) < 0)) {
		*(uintptr_t*)(clientBase + dwForceJump) = 6;
	}
}