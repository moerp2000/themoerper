#include <iostream>

#include "triggerbot.h"


using namespace std;

extern uintptr_t clientBase;

void triggerbot() {
	uintptr_t localPlayer = *(uintptr_t*)(clientBase + dwLocalPlayer);
	if (localPlayer) {
		uintptr_t crosshair = *(uintptr_t*)(localPlayer + m_iCrosshairId);
		uintptr_t targetEntity = *(uintptr_t*)(clientBase + dwEntityList + (crosshair - 1) * 0x10);

		if (targetEntity != 0) {
			int localPlayerTeam = *(int*)(localPlayer + m_iTeamNum);
			int targetEntityTeam = *(int*)(targetEntity + m_iTeamNum);
			int targetEntityHealth = *(int*)(targetEntity + m_iHealth);

			if (targetEntityTeam != localPlayerTeam && targetEntityHealth > 0) {
				*(uintptr_t*)(clientBase + dwForceAttack) = 5;
				Sleep(25);
				*(uintptr_t*)(clientBase + dwForceAttack) = 4;
				Sleep(300);
			}
		}
	}
}