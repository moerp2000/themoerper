#include "wallhack.h"

using namespace std;

extern uintptr_t clientBase;


const float glowColorR = 2.0;
const float glowColorG = 0.0;
const float glowColorB = 0.0;


void glowHack() {
	uintptr_t localPlayer = *(uintptr_t*)(clientBase + dwLocalPlayer);
	if (localPlayer) {
		int localPlayerTeam = *(int*)(localPlayer + m_iTeamNum);
		uintptr_t glowObj = *(uintptr_t*)(clientBase + dwGlowObjectManager);

		for (int i = 0; i < maxPlayer; i++) {
			uintptr_t entity = *(uintptr_t*)(clientBase + dwEntityList + i * 0x10);

			if (entity != 0) {
				bool isDormant = *(bool*)(entity + m_bDormant);

				if (!isDormant) {
					int entityHealth = *(int*)(entity + m_iHealth);
					int entityTeam = *(int*)(entity + m_iTeamNum);

					if (entityTeam != localPlayerTeam && entityHealth > 0) {
						uintptr_t curGlowIndex = *(uintptr_t*)(entity + m_iGlowIndex);

						*(float*)(glowObj + curGlowIndex * 0x38 + 0x4) = glowColorR;
						*(float*)(glowObj + curGlowIndex * 0x38 + 0x8) = glowColorG;
						*(float*)(glowObj + curGlowIndex * 0x38 + 0xC) = glowColorB;
						*(float*)(glowObj + curGlowIndex * 0x38 + 0x10) = 1.0;
						*(bool*)(glowObj + curGlowIndex * 0x38 + 0x24) = true;
						*(bool*)(glowObj + curGlowIndex * 0x38 + 0x25) = false;
					}
				}
			}
		}
	}
}