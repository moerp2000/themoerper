#include <Windows.h>
#include <stdio.h>
#include <iostream>

#include "dx.h"
#include "sdk.h"
#include "offsets.h"
#include "triggerbot.h"
#include "aimbot.h"
#include "wallhack.h"
#include "skinchanger.h"
#include "bhop.h"
#include "antiRecoil.h"

using namespace std;

uintptr_t clientBase;
uintptr_t engineBase;

FrameStageNotify oFrameStageNotify = nullptr;

bool triggerToggle = false;
bool whToggle = false;
bool bhobToggle = false;
bool rsToggle = false;
bool aimbotToggle = false;
bool anitRecoilToggle = false;

int EJECT_KEY = VK_END;

void* d3d9Device[119];
BYTE EndSceneBytes[7]{ 0 };
tEndScene oEndScene = nullptr;
extern LPDIRECT3DDEVICE9 pDevice = nullptr;


void toggleFeatures() {
    if (GetKeyState(VK_NUMPAD2) < 0 && triggerToggle == false) {
        triggerToggle = true;
        cout << "Triggerbot on" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_NUMPAD2) < 0 && triggerToggle == true) {
        triggerToggle = false;
        cout << "Triggerbot off" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_NUMPAD0) < 0 && whToggle == false) {
        whToggle = true;
        cout << "Wallhack on" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_NUMPAD0) < 0 && whToggle == true) {
        whToggle = false;
        cout << "Wallhack off" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_NUMPAD3) < 0 && bhobToggle == false) {
        bhobToggle = true;
        cout << "Bhop on" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_NUMPAD3) < 0 && bhobToggle == true) {
        bhobToggle = false;
        cout << "Bhop off" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_MULTIPLY) < 0 && rsToggle == false) {
        rsToggle = true;
        cout << "Random skinchanger on" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_MULTIPLY) < 0 && rsToggle == true) {
        rsToggle = false;
        cout << "Random skinchanger off" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_NUMPAD1) < 0 && aimbotToggle == false) {
        aimbotToggle = true;
        cout << "Aimbot on" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_NUMPAD1) < 0 && aimbotToggle == true) {
        aimbotToggle = false;
        cout << "Aimbot off" << endl;
        Sleep(50);
    }else if (GetKeyState(VK_NUMPAD4) < 0 && anitRecoilToggle == false) {
        anitRecoilToggle = true;
        cout << "antiRecoil on" << endl;
        Sleep(50);
    }
    else if (GetKeyState(VK_NUMPAD4) < 0 && anitRecoilToggle == true) {
        anitRecoilToggle = false;
        cout << "anitRecoil off" << endl;
        Sleep(50);
    }
}

void APIENTRY hkEndScene(LPDIRECT3DDEVICE9 o_pDevice) {
    if (!pDevice)
        pDevice = o_pDevice;

    oEndScene(pDevice);
}


void _stdcall hkFrameStageNotify(ClientFrameStage_t curStage) {
	if (curStage == FRAME_NET_UPDATE_POSTDATAUPDATE_START) {
		skinchanger();
	}
	oFrameStageNotify(curStage);
}


DWORD WINAPI NoDelayThread(HMODULE hModule) {
	cout << "NoDelayThread running has been started" << endl;
	tCreateInterface Client1 = (tCreateInterface)GetProcAddress(GetModuleHandleA("client.dll"), "CreateInterface");
	void* Client = Client1("VClient018", NULL);
	uintptr_t* vTable = (*reinterpret_cast<uintptr_t**>(Client));
	oFrameStageNotify = (FrameStageNotify)TrampHook((char*)vTable[37], (char*)hkFrameStageNotify, 9);
    
	while (!GetAsyncKeyState(EJECT_KEY)) {
		// aim & shoot functions
        if (triggerToggle) {
            triggerbot();
        }
        if (anitRecoilToggle) {
            antiRecoil();
        }
        if (aimbotToggle) {
            aimbot();
        }
        
	}
	return 0;
}


DWORD WINAPI WallhackThread(HMODULE hModule) {
	cout << "Wallhack running has been started" << endl;

	while (!GetAsyncKeyState(EJECT_KEY)) {
		// wallhack functions
        if (whToggle) {
            glowHack();
        }
        if (bhobToggle) {
            bhop();
        }
		Sleep(2);
	}
    return 0;
}


DWORD WINAPI MainThread(HMODULE hModule) {
    // Hook EndScene
    if (GetD3D9Device(d3d9Device, sizeof(d3d9Device))) {
        memcpy(EndSceneBytes, (char*)d3d9Device[42], 7);

        oEndScene = (tEndScene)TrampHook((char*)d3d9Device[42], (char*)hkEndScene, 7);
    }

    // Hook FrameStageNotify
    tCreateInterface Client1 = (tCreateInterface)GetProcAddress(GetModuleHandleA("client.dll"), "CreateInterface");
    void* Client = Client1("VClient018", NULL);
    uintptr_t* vTable = (*reinterpret_cast<uintptr_t**>(Client));

    oFrameStageNotify = (FrameStageNotify)TrampHook((char*)vTable[37], (char*)hkFrameStageNotify, 9);
}



BOOL APIENTRY DllMain(HMODULE hModule, DWORD reason, LPVOID lpReserved) {

	AllocConsole();
	FILE* f;
	freopen_s(&f, "CONOUT$", "w", stdout);
	clientBase = (uintptr_t)GetModuleHandle(L"client.dll");
	engineBase = (uintptr_t)GetModuleHandle(L"engine.dll");


	switch (reason) {
	case DLL_PROCESS_ATTACH:
		CloseHandle(CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)NoDelayThread, hModule, 0, nullptr));
		Sleep(5);
		CloseHandle(CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)WallhackThread, hModule, 0, nullptr));
		Sleep(5);
        while (!GetAsyncKeyState(EJECT_KEY)) {
            toggleFeatures();
            Sleep(50);
        }
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}

	return TRUE;
}